clear all

cd "I:\Personal Files\Jim\Stata\Git\HaversineDistance"
do "HaversineDistance.ado"

set seed 1234567890
set obs 20
gen lat1 = runiform()*180 - 90
gen lat2 = runiform()*180 - 90
gen lon1 = runiform()*360 - 180
gen lon2 = runiform()*360 - 180
gen temp = runiform()
gen touse1 = round(runiform())
gen touse2 = round(runiform())
gen id = _n

matrix A = 43,83,1 \ 43, 87,2 \ 47,83,4 \ 47,87,9
matrix colnames A = "latitude" "longitude" "temp"

HaversineDistance blah = CountWithin(1000) ///
	blah2= DistanceToClosest() ///
	blah3 = MeanDistance() ///
	blah4 = WeightedMeanWithin(1000, temp) ///
	blah5 = closestNthIndex(1,id) ///
	blah6 = distanceToNthClosest(1) ///
	blah7 = distanceToNthClosest(2) ///
	if touse1, geofrom(lat1 lon1) geotovar(lat2 lon2) geotoselect(touse2) units(mi)


HaversineDistance foo1 = CountWithin(100) ///
	foo2 = SumWithin(100,temp) ///
	foo3 = MeanDistance() if touse1, geofrom(lat1 lon1) geotomatrix(A) units(mi)

preserve
	keep if touse2 == 1
	tempfile xxx
	save `xxx'
restore

HaversineDistance bar1 = CountWithin(100) ///
	bar2 = SumWithin(100,temp) ///
	bar3 = MeanDistance() ///
	bar4 = WeightedMeanWithin(1000,temp) ///
	bar5 = closestNthIndex(1,id) ///
	using `xxx' if touse1 , geofrom(lat1 lon1) usingvarlist(lat2 lon2)

**Bad file
tempfile xxx1
capture noisily HaversineDistance bar1 = CountWithin(100) ///
	bar2 = SumWithin(100,temp) ///
	bar3 = MeanDistance() using `xxx1' if touse1 , geofrom(lat1 lon1) usingvarlist(lat2 lon2)
assert _rc == 601 //The program should throw a file not found error

