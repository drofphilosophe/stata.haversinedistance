/********************************************
** HaversineDistance
**

Copyright 2016 James Archsmith

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


** This program calculates statistics on the distance using latitude and longitude 
** from locations defined by observations in the active dataset and locations defined in a matrix
** The general syntax of a call to the program is 
**
** HaversineDistance statspec [if] [in], geofrom(<LatitudeVar> <LongitudeVar>) geoto(<LocationMatrix>) [units(<UnitName>)]
** 
** Where statspec is of the form:
** <newvarname> = <function>([arguments])
** 
** <LatitudeVar> and <LongitudeVar> are numeric variables containing the geographic corrdinates in degrees
** <LocationMatrix> is a matrix with latitudes in the first column and longitudes in the second (in degrees)
** Row names can optionally be defined and may be used by some functions
** <UnitName> is the abrivation of a the distance units to use.  Currenly "km" and "mi" are supported.
**
** The function performs calcualtions in Mata to improve performance and conserve memory. There is a persistent
** matrix defined when the program is loaded. (HD_DISTANCE_ARRAY). It is cleaned up by proper exiting of the program.
**
** Sample code is included at the bottom of the program.
**
** The distance calculation assumes that the Earth is an elipsoid.  This should be accruate enough for most applications.
** A more accurate calculation would assume that the earth is an oblate spheroid.  Those calculations would 
** be more difficult to implement.
*********************************************/
**clear all
mata

class HaversineDistance {
	//Member data
	real matrix distance_array
	real matrix loc1
	real matrix loc2
	real scalar Nloc1	//Number of observations in Loc1
	real scalar Nloc2	//Number of observations in Loc2
	string units
	string loc1Select		//Varname of the indicator that is true for loc1 observations included in the calculations
	string loc2Select
	string matrix loc2names	//Column names of the loc2 matrix
	
	//member methods
	/* INITIALIZATION METHODS */
	void new()		//initialize the class
	//Set the "location 1" variables. These should be variables in the dataset. Distances will be written to new variables that corrispond with these observations.
	void setLoc1Vars()
	//Set the "location 2" variables using observations in the dataset. Other variables to be 
	//included for distance calcualtions may need to be specified here
	void setLoc2Vars()
	//Set the "location 2" variables (and associated parameters) using an external data file
	void setLoc2DataFile()
	//Set the "location 2" variables (and associated parameters) using a matrix
	void setLoc2Matrix()
	//Add a variable to Loc2
	real scalar addLoc2Variable()
	//Get the index of a column in Loc2
	real scalar getLoc2Index()
	
	//Set the units in which to express distances
	void setUnits()
	
	/* CalcDistance creates a matrix of distances between Loc1 (rows) and Loc2 (columns) */
	void calcDistance()
	
	/* Define a number of member functions which compute statistics based on the distance array 
	   It is expected that these functions write results to the Stata dataset */
	//Count the number of Loc2 within X units of Loc1
	void countLessThan()
	//Sum variable Y for all Loc2 observations within X units of Loc1
	void sumWithin()
	//Return the distance in units to the Loc2 observation that is closest to Loc1.
	void closestDistance()
	//Return the mean distance to units in Loc2 from Loc1
	void meanDistance()
	//Return the distance-weighted average of a variable to Loc2 from Loc1
	void distanceWeightedMean()
	//Find the index of the nth-closet unit in Loc2 from Loc1
	void closestNthIndex()
	//Find the distance to the nth-closest unit in Loc2 from Loc1
	void distanceToNthClosest()
	//Add other member functions as needed
	
	/* The destructor */
	void destroy()
}

/* Init the class */
void function HaversineDistance::new() {
	//Set default units
	units = "mi"

	//Anything else?
}

void function HaversineDistance::setLoc1Vars(string loc1Latitude, loc1Longitude, string selectVar) {
	loc1 = st_data(., ( loc1Latitude, loc1Longitude ), selectVar)
	loc1Select = selectVar
	Nloc1 = rows(loc1)
}

void function HaversineDistance::setLoc2Vars(string loc2Latitude, loc2Longitude, string colvector otherVars, string selectVar) {
	if(otherVars != .) {
		loc2 = st_data(., (loc2Latitude, loc2Longitude, otherVars), selectVar)
	}
	else {
		loc2 = st_data(., (loc2Latitude, loc2Longitude), selectVar)
	}
	loc2Select = selectVar
	Nloc2 = rows(loc2)
}

//Set the "location 2" variables (and associated parameters) using an external data file
void function HaversineDistance::setLoc2DataFile(string loc2Latitude, string loc2Longitude, string filename) {
	real rowvector keepnames
	string tfile
	
	printf("{txt}Loading stata data file " + filename + " as {bf:geoto} dataset\n")
	
	//Create a temporary file to hold the current dataset.
	tfile = st_tempfilename()
	//Save the current data to a tempfile, suppress output
	stata("save " + tfile, 1)
	//Load the file the user specified. The wrapper program checks that the file exists.
	stata("use " + filename + ",clear")
	
	//Get the varnames from the loaded file
	loc2names = st_varname(1 .. st_nvar())
	//Remove the varnames of the latitiude and longitude vars
	keepnames = (loc2names :!= loc2Latitude) :& (loc2names :!= loc2Longitude)
	//Construct a vector of varnames with latitude and longitude first
	loc2names = ( loc2Latitude, loc2Longitude, select(loc2names, keepnames) )
	loc2 = st_data(., loc2names)
	//Now that we have the data that the user specified, reload the original data
	stata("use " + tfile + ", clear")
	
	printf("{txt}Loaded %10.0f variables and %10.0f observations\n\n", cols(loc2), rows(loc2) )
	//count the number of rows
	Nloc2 = rows(loc2)

	//The program expectes stata colstripe-style varnames.  
	//Rearrange loc2names to meet this format
	loc2names = J(cols(loc2names), 1, "") , loc2names'
}

//Set the "location 2" variables (and associated parameters) using a matrix
void function HaversineDistance::setLoc2Matrix(string loc2matrix) {
	loc2 = st_matrix(loc2matrix)
	loc2names = st_matrixcolstripe(loc2matrix)
	Nloc2 = rows(loc2)
}
	
//Set the units in which to express distances
void function HaversineDistance::setUnits(string unitsName) {
	if(lower(unitsName) == "mi" | lower(unitsName) == "miles") {
		units = "mi"
	}
	if(lower(unitsName) == "km" | lower(unitsName) == "kilometers") {
		units = "km"
	}
	else {
		printf("{err}Unknown unit type\n")
	}
}

/* This function will add a column to loc2. These are intended to be
   additional variables on which some distance-based calculatons will be run.
	It returns the column index of the added data.
   */
real scalar HaversineDistance::addLoc2Variable(string varname) {
		loc2 = loc2 , st_data(., varname, loc2Select)
		return(cols(loc2))
}

real scalar HaversineDistance::getLoc2Index(string colname) {
	for(i=1;i<=rows(loc2names);i++) {
		if(loc2names[i,2] == colname) {
			return(i)	//Program will bail here if it finds the correct column name
		}
	}
	printf("{err}%s not found\n", colname)
	exit(3012)
}
/* Actual Haversine distance calculations. This needs to be called before any of the magic can happen */
void HaversineDistance::calcDistance() {
	/* Convert lat and lng to radians */
	lat1 = loc1[.,1] / 180 * pi()
	lng1 = loc1[.,2] / 180 * pi()
	lat2 = loc2[.,1] / 180 * pi()
	lng2 = loc2[.,2] / 180 * pi()
	
	/*create matrixes of the difference in latitude and longitude*/
	dLat = abs(lat1*J(1,Nloc2,1) - J(Nloc1,1,1)*(lat2') )
	dLng = abs(lng1*J(1,Nloc2,1) - J(Nloc1,1,1)*(lng2') )

	/* Compute Haversine distance in km */
	/* See http://www.movable-type.co.uk/scripts/gis-faq-5.1.html */
	A = sin(dLat :/ 2):^2 + ( cos(lat1) * cos(lat2)' ) :* sin(dLng:/2):^2
	B = A:^0.5
	/* Take the min of each element of B and 1 */
	B1 = (B:<=1):*B + (B:>1):*J(Nloc1,Nloc2,1)
	C = 2 * asin( B1 )
	if(units == "km") {
		R = 6378 :- 21 :* sin( (lat1*J(1,Nloc2,1) + J(Nloc1,1,1)*(lat2') ) :/ 2)
	}
	else if(units == "mi") {
		R = 3963 :- 13 :* sin( (lat1*J(1,Nloc2,1) + J(Nloc1,1,1)*(lat2') ) :/ 2)
	}
	
	/* Set the member object distance_array to the matrix of distances */
	distance_array = R :* C
}

/* This function returns the number of entries in LOC2 that are within THRESHOLD km of the 
   corrisponding LOC1
*/
void HaversineDistance::countLessThan(real scalar threshold, string outputVarName) {
	real matrix boolMat
	colvector outputVar
	
	st_view(outputVar, ., outputVarName, loc1Select)
	
	boolMat = distance_array :<= threshold
	outputVar[.,.] = boolMat*J(Nloc2,1,1)
}

/* This function computes the sum of sumIndex columns of Loc2 for all observations within threshold and saves
   it in outputVarName */
void function HaversineDistance::sumWithin(real scalar threshold, string outputVarName, real scalar sumIndex) {
	real matrix boolMat
	colvector sumVarData
	colvector outputVar
	
	st_view(outputVar, ., outputVarName, loc1Select)
	sumVarData = loc2[.,sumIndex]	
	boolMat = distance_array :<= threshold
	outputVar[.,.] = boolMat*sumVarData
}

/* This function computes the distance-weighted sum of the sumIndex-th column of observations within threshold
   and saves the result in outputVarName */
void function HaversineDistance::distanceWeightedMean(real scalar threshold, string outputVarName, real scalar sumIndex) {
	real matrix weightMat
	real colvector sumOfWeights
	colvector sumVarData
	colvector outputVar
	
	st_view(outputVar, ., outputVarName, loc1Select)
	sumVarData = loc2[.,sumIndex]
	
	//Generate an NLoc1xNLoc2 matrix that has a weight for each entry that is the reciprical of distance
	weightMat = (distance_array :<= threshold) :* ( distance_array )
	//What if the distance is zero? Make the weight some small, non-zero number. I'm using epsilon(1), it is about 10^-34
	weightMat = weightMat + (distance_array :== 0) :* epsilon(1)
	//Now take the recprical of each entry in weightMat so it is the inverse distance
	weightMat = weightMat :^ -1
	
	//Change weights to zero if the corrisponding value of sumVarData is missing
	for(i=1;i<=Nloc2;i++) {
		if(sumVarData[i] >= . ) {
			weightMat[.,i] = J(Nloc1,1,0)
		}
	}
	
	//Compute the sum of the weights
	sumOfWeights = rowsum(weightMat)
	//Multiply each weight by the appropriate value in sumVarData
	weightMat = weightMat :* sumVarData'

	outputVar[.,.] = rowsum(weightMat) :/ sumOfWeights 
}
/* This function returns the distance to LOC2 that is closest to the 
   corrisponding LOC1. 
   I could envison changing it to that it returns the distance to the 
   Nth closest LOC2. An additional function may return the matrix rowname
   of the Nth closest LOC2.
*/
void function HaversineDistance::closestDistance(string outputVarName) {
	colvector outputVar
	
	st_view(outputVar, ., outputVarName, loc1Select)
	outputVar[.,.] = rowmin(distance_array)
}

/* Return the mean distance from LOC1 to all of the points in LOC2 */
void function HaversineDistance::meanDistance(string outputVarName) {
	colvector outputVar
	
	st_view(outputVar, ., outputVarName, loc1Select)
	outputVar[.,.] = mean(distance_array')'
}

/* Return the distance to the entry in LOC2 that is the Nth closest to each LOC1 */
void function HaversineDistance::distanceToNthClosest(real scalar nth, string outputVarName) {
	colvector outputVar
	colvector idxs
	
	i = .
	w = .
	
	st_view(outputVar, ., outputVarName, loc1Select)
	
	distances = J(rows(outputVar),1,.)
		
	for(j=1;j<=rows(distance_array);j++) {
		minindex(distance_array[j,.],nth,i,w)
		distances[j] = distance_array[j, i[nth] ]
	}
	outputVar[.,.] = distances
}

/* Return the index of the entry in LOC2 that is the Nth closest to each LOC1 */
void function HaversineDistance::closestNthIndex(real scalar indexVar, real scalar nth, string outputVarName) {
	colvector outputVar
	colvector idxs
	
	i = .
	w = .
	
	st_view(outputVar, ., outputVarName, loc1Select)
	indexVarCol = loc2[.,indexVar]
	
	idxs = J(rows(outputVar),1,.)
		
	for(j=1;j<=rows(distance_array);j++) {
		minindex(distance_array[j,.],nth,i,w)
		idxs[j] = i[nth] 
	}
	outputVar[.,.] = indexVarCol[idxs]
}
/* This function destroys the array of computed Haversine distances. 
   The matrix could potentally be quite large and would have a significant memory
   footprint. Calling this function frees the memory.
   It should be called before the Stata wrapper exits. There are multiple exit
   points from the Stata wrapper (e.g., due to invalid syntax) so be careful
   to call prior to every exit command.
*/
void function HaversineDistance::destroy() {
	distance_array = .
	loc1 = .
	loc2 = .
	//Do we need to do anything? Garbage collection should take care of all internals when the class
	//Is destroyed. The only thing would be to close file handles and the like.  I think
	//that should be all good.
}

end

/*clear
set obs 20
gen lat1 = rnormal()
gen lat2 = rnormal()
gen lon1 = rnormal()
gen lon2 = rnormal()
gen temp = runiform()
gen touse1 = round(runiform())
gen touse2 = round(runiform())
gen meanDistance = .
gen countLessThan = .
gen sumTemp = .
gen closestDistance = .

mata
	H = HaversineDistance()
	H.setLoc1Vars("lat1", "lon1", "touse1")
	H.setLoc2Vars("lat2", "lon2", "", "touse2")
	H.addLoc2Variable("temp")
	H.calcDistance()
	H.countLessThan(100, "countLessThan")
	H.meanDistance("meanDistance")
	H.closestDistance("closestDistance")
	H.sumWithin(100, "sumTemp", 3)
	
end
	
STOP
*/

program define HaversineDistance, rclass
syntax anything(equalok) [using/] [if] [in] , GEOFrom(varlist min=2 max=2 numeric) [GEOTOMATrix(namelist max=1) ///
	GEOTOVARlist(varlist min=2 max=2) GEOTOSELect(varname) units(string) float USINGVARlist(namelist min=2 max=2)]
	
	/* Say hello */
	display as result _n "Haversine Distance Calculations"
	
	/* If the user specified FLOAT gen floats. Othersise doubles */
	if "`float'" == "float" {
		local format float
		display as text "Generating statistics in single precision (float)"
	}
	else {
		local format double
	}
	
	/* User should specify either GEOTOMATRIX, using, or GEOTOVARLIST */
	if !missing("`geotomatrix'") + !missing("`using'") + !missing("`geotovarlist'") != 1 {
		display as error "You must specify exactly one of using, geotomatrix(), or geotovarlist()"
		exit 99
	}
	
	/* Set geotomode depending on the method used to load geoto data */
	if "`geotomatrix'" != "" scalar geotomode = 1
	else if "`using'" != "" scalar geotomode = 2
	else if "`geotovarlist'" != "" scalar geotomode = 3
	
	/* If "units" is not specified, default to km. Otherwise, make sure the
	   Units specified in the argument are in the allowed list */
	if "`units'" == "" {
		local units "km"
	}
	else if !inlist("`units'", "km", "mi") {
		di as error "`units' is an invalid distance unit."
		exit 99
	}
	/* Say which units we picked */
	di as text "Units in `units'"
	
	preserve
	tempvar touse
	mark `touse' `if' `in'
	markout `touse' `geofrom'
	
	tempname H		//Temporary name for the mata object that performs calculations
	
	**Compute the Haversine Distance matrix
	mata:`H' = HaversineDistance()
	tokenize `geofrom'
	**Do some sanity checking. Latitudes should be from -90 to +90
	capture assert inrange(`1', -90, 90) if `touse'
	if _rc != 0 {
		display as error "Variable `1' does not appear to be latitude data."
		exit 459
	}
	capture assert inrange(`2', -180, 180) if `touse'
	if _rc != 0 {
		display as error "Variable `2' does not appear to be longitude data."
		exit 459
	}
	
	mata:`H'.setLoc1Vars("`1'", "`2'", "`touse'")
	
	if "`geotomatrix'" != "" {
		display as text "Using matrix " as result "`geotomatrix'" as text " as destinations"
		mata:`H'.setLoc2Matrix("`geotomatrix'")
	}
	else if "`geotovarlist'" != "" {
		tokenize `geotovarlist'
		display as text "Distance to observations"
		display as text "Latitude: " as result "`1'"
		display as text "Longitude: " as result "`2'"
		display as text "Select: " as result "`geotoselect'"
		capture assert inrange(`1', -90, 90) if `geotoselect'
		if _rc != 0 {
			display as error "Variable `1' does not appear to be latitude data."
			exit 459
		}
		capture assert inrange(`2', -180, 180) if `geotoselect'
		if _rc != 0 {
			display as error "Variable `2' does not appear to be longitude data."
			exit 459
		}
		local lat2 `1'
		local lon2 `2'
		macro shift 2
		mata: `H'.setLoc2Vars("`lat2'", "`lon2'", "`*'", "`geotoselect'")
	}
	else if "`using'" != "" {
		capture confirm file "`using'"
		if _rc != 0 {
			di as error "File " as result "`using'" as error " does not exist."
			exit 601	//File not found error
		}
		
		tokenize `usingvarlist'
		
		mata: `H'.setLoc2DataFile("`1'", "`2'", "`using'")
	}
	
	**Calculate distances
	mata: `H'.calcDistance()
	
	**Parse out and generate the statistics specified by the user
	**ANYTHING should take the form of newvarname = function(arg) ...
	**Remove spaces so that everything tokenizes correctly. Each token should be
	**"<newvarname>=<fncall>([fnargs])"
	local anything : subinstr local anything " " "", all
	local anything : subinstr local anything ")" ") ", all
	
	
	**Now, parse out the commands.
	**Currentl valid functions are:
	** CountWithin(Distance)
	** DistanceToClosest()
	** MeanDistance()
	** Other Ideas - IndexOf[Nth]Closest()
	** SDDistance()
	
	tokenize "`anything'"

	**Loop through each token
	while "`1'" != "" {
		**Parse the token into components <newvarname>=<fncall>([fnargs])
		if regexm("`1'", "^([^=]+)=([^\(]+)\(([^\)]*)\)$") {
			local newvarname = regexs(1)
			local fncall = regexs(2)
			local fnargs = regexs(3)
			
			**Check that the specified variable does not already exist
			**If it does, bail
			capture confirm new var `newvarname'
			if _rc != 0 {
				di as error "`newvarname' is not a new variable name."
				mata:`H' = .
				exit 198
			}
			
			**take the required action for the specified function
			
			***************************************
			** CountWithin
			***************************************
			if "`fncall'" == "CountWithin" {
				if regexm("`fnargs'", "^[0-9\.]+$") {
						di as text "Generating " as result "`newvarname'" as text " as count of points within `fnargs' `units'"
						qui gen long `newvarname' = .
						mata:`H'.countLessThan(`=real("`fnargs'")', "`newvarname'")
				}
				else {
					di as error "Argument to CountWithin must be a non-negative integer."
					mata:`H' = .
					exit 109	//Type mismatch
				}
			}
			
			*************************************
			** DistanceToClosest
			** DEPRECATED - Use distanceToNthClosest(1) instead
			*************************************
			else if "`fncall'" == "DistanceToClosest" {
				if "`fnargs'" == "" {
					di as text "Generating " as result "`newvarname'" as text " as distance to closest point in `units'"
					
					**Warn the user this function is deprecated
					di "{txt}{bf:WARNING}: The function {bf:DistanceToClosest()} has been deprecated and may disappear from " _continue
					di "{txt}future versions." _n "Use {bf:distanceToNthClosest(1)} instead."
					
					qui gen `format' `newvarname' = .
					mata:`H'.closestDistance("`newvarname'")
				}
				else {
					di as error "DistanceToClosest takes no arguments."
					mata:`H' = .
					exit 109	//Type mismatch
				}
			}
			
			**********************************
			** MeanDistance
			**********************************
			else if "`fncall'" == "MeanDistance" {
				if "`fnargs'" == "" {
					di as text "Generating " as result "`newvarname'" as text " as mean distance in `units'"
					qui gen `format' `newvarname' = .
					mata:`H'.meanDistance("`newvarname'")
				}
				else {
					di as error "MeanDistance takes no arguments."
					mata:`H' = .
					exit 109	//Type mismatch
				}
			}
			**********************************
			** SumWithin
			**********************************
			else if "`fncall'" == "SumWithin" {
				if regexm("`fnargs'", "([0-9\.]+) *, *([A-Za-z_0-9]+)") {
					local threshold `=regexs(1)'
					local sumVar `=regexs(2)'
					
					di as text "Generating " as result "`newvarname'" as text " as sum of " as result "`sumVar'" as text " within `threshold' `units'"
					qui gen `format' `newvarname' = .
					
					//If geotomode == 3 we need to add avariable from the dataset
					if geotomode == 3 {
						mata:`H'.sumWithin(`threshold', "`newvarname'", `H'.addLoc2Variable("`sumVar'"))
					}
					else {
						mata:`H'.sumWithin(`threshold', "`newvarname'", `H'.getLoc2Index("`sumVar'"))
					}
				}
				else {
					di as error "SumWith takes a distance and a numeric variable as arguments."
					mata:`H' = .
					exit 109	//Type mismatch
				}
			}
			**********************************
			** WeightedMeanWithin
			**********************************
			else if "`fncall'" == "WeightedMeanWithin" {
				if regexm("`fnargs'", "([0-9\.]+) *, *([A-Za-z_0-9]+)") {
					local threshold `=regexs(1)'
					local meanVar `=regexs(2)'
					
					di as text "Generating " as result "`newvarname'" as text " as the inverse-distance weighted mean of " as result "`meanVar'" as text " within `threshold' `units'"
					qui gen `format' `newvarname' = .
					//Need to fix this call. We either need to add sumVar to the matrix, or we need to
					//lookup the column number of `sumVar'
					
					//If geotomode == 3 we need to add avariable from the dataset
					if geotomode == 3 {
						mata:`H'.distanceWeightedMean(`threshold', "`newvarname'", `H'.addLoc2Variable("`meanVar'"))
					}
					else {
						mata:`H'.distanceWeightedMean(`threshold', "`newvarname'", `H'.getLoc2Index("`meanVar'"))
					}
				}
				else {
					di as error "WeightedMeanWithin takes a distance and a numeric variable as arguments."
					mata:`H' = .
					exit 109	//Type mismatch
				}
			}
			
			****************************************
			** closestNthIndex(Nth,IndexVarname)
			****************************************
			else if "`fncall'" == "closestNthIndex" {
				if regexm("`fnargs'", "([0-9\.]+) *, *([A-Za-z_0-9]+)") {
					local nth `=regexs(1)'
					local index `=regexs(2)'
					
					di as text "Generating " as result "`newvarname'" as text " as the value of {res}`index' " _continue
					di as text "for the {res}`nth'th {txt}closest observation"
					
					qui gen double `newvarname' = .
					
					//If geotomode == 3 we need to add avariable from the dataset
					if geotomode == 3 {
						mata:`H'.closestNthIndex(`H'.addLoc2Variable("`index'"), `nth', "`newvarname'")
					}
					else {
						mata:`H'.closestNthIndex(`H'.getLoc2Index("`index'"), `nth', "`newvarname'")
					}
				}
				else {
					di as error "closestNthIndex takes an integer and a numeric index variable as arguments."
					mata:`H' = .
					exit 109	//Type mismatch
				}				
			}
			****************************************
			** distanceToNthClosest(Nth)
			****************************************
			else if "`fncall'" == "distanceToNthClosest" {
				if regexm("`fnargs'", "([0-9]+)") {
					local nth `=regexs(1)'
					
					di as text "Generating " as result "`newvarname'" as text " as the distance to " _continue
					di as text "the {res}`nth'th {txt}closest observation"
					
					qui gen double `newvarname' = .
					
					mata:`H'.distanceToNthClosest(`nth', "`newvarname'")
				}
				else {
					di as error "distanceToNthClosest takes an integer and a numeric index variable as arguments."
					mata:`H' = .
					exit 109	//Type mismatch
				}				
			}			
			****************************************
			** Function Not found
			****************************************
			else {
				*We should throw an error, clean up ane exit if we reach this section
				*This is, however, still debug code. We'll change it later.
				di as text "Unused Argument: `newvarname' = `fncall'(`fnargs')"
				mata:`H' = .
				exit 133	//Unknown Function
			}
			
		}
		********************************
		** Parsing Error
		********************************
		else {
			di as error "Arguments must be of the form <newvarname>=<functionname>([args])"
			di as error "Offending argument: " as result "`1'"
			mata:`H' = .
			exit 133	//Unknown function
		}
		
		macro shift
	}
	
	mata:`H' = .
	restore, not
end

/*
clear
set seed 1234567890
set obs 20
gen lat1 = runiform()*180 - 90
gen lat2 = runiform()*180 - 90
gen lon1 = runiform()*360 - 180
gen lon2 = runiform()*360 - 180
gen temp = runiform()
gen touse1 = round(runiform())
gen touse2 = round(runiform())

matrix A = 43,83,1 \ 43, 87,2 \ 47,83,4 \ 47,87,9
matrix colnames A = "latitude" "longitude" "temp"

HaversineDistance blah = CountWithin(1000) ///
	blah2= DistanceToClosest() ///
	blah3 = MeanDistance() ///
	blah4 = WeightedMeanWithin(1000, temp) ///
	if touse1, geofrom(lat1 lon1) geotovar(lat2 lon2) geotoselect(touse2) units(mi)

STOP
drop blah
HaversineDistance blah = CountWithin(100) ///
	foo3 = SumWithin(100,temp) ///
	foo2 = MeanDistance() if touse1, geofrom(lat1 lon1) geotomatrix(A) units(mi)
*/
