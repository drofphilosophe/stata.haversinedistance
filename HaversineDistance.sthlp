{smcl}
{* *! version 1.2.1  07mar2013}{...}
{findalias asfradohelp}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[R] help" "help help"}{...}
{viewerjumpto "Syntax" "examplehelpfile##syntax"}{...}
{viewerjumpto "Description" "examplehelpfile##description"}{...}
{viewerjumpto "Options" "examplehelpfile##options"}{...}
{viewerjumpto "Remarks" "examplehelpfile##remarks"}{...}
{viewerjumpto "Examples" "examplehelpfile##examples"}{...}
{title:Title}

{phang}
{bf:HaversineDistance} {hline 2} Compute distance-based statistics between points on the surface of the 
	Earth using latitudes and longitudes


{marker syntax}{...}
{title:Syntax}

{p 8 17 2}
{cmdab:HaversineDistance}
[{it:clist}]
[using {help filename}]
{ifin}
{cmd:,} {opt geof:rom}({it:Latvar} {it:Longvar}) [{it:options}]

{pstd} where {it:clist} is 

{p 8 17 2}
{help newvarname}={it:function}([{it:arguments}]) [{help newvarname}={it:function}([{it:arguments}]) ...]{p_end}

{p 8 17 2}
and {it:function}([{it:arguments}]) is any of the following:

{p2colset 9 20 20 2}{...}
{p2col :{opt closestNthIndex(N,dataselect)}}The value of variable {it:dataselect} for the {it:N}th-closest observation. 
	{it:newvar} will be of the same type as {it:dataselect} regardless of the setting of {bf:float}.{p_end}
{p2col :{opt CountWithin(dist)}}Count of observations within {it:dist}{p_end}
{p2col :{opt distanceToNthClosest(N)}}Distance to the {it:N}th-closest observation{p_end}
{p2col :{opt MeanDistance()}}Mean distance to all observations{p_end}
{p2col :{opt SumWithin(dist,dataselect)}}Sum of variable {it:dataselect} for all observations within {it:dist}{p_end}
{p2col :{opt WeightedMeanWithin(dist,dataselect)}}Inverse distance weighted mean of variable {it:dataselect} for all obsevations within {it:dist}{p_end}


{pstd}{it:dist} is a distance in {bf:units()} between points in {bf:geofrom} and {bf:geoto}. A missing value for {it:dist} causes
	the distance criterion to be ignored{p_end}
{pstd}{it:dataselect} is either a {it:varname} in the {bf:geoto} data or a column number if the {bf:geoto} data are in a matrix{p_end}


{synoptset 20 tabbed}{...}
{synopthdr}
{synoptline}
{syntab:Main}
{synopt:{opt units(string)}}Specify either {bf:km} or {bf:mi} (default) use kilometers or miles, respectively{p_end}
{synopt:{opt float}}Generated statistics will be floats instead of doubles.


{syntab:{bf:geoto} data in the current dataset}
{synopt:{opt geotovar:list(Latvar Longvar)}}Latitude and longitude varnames to compute distance to{p_end}
{synopt:{opt geotosel:ect(varname)}}Only include observations where {it:varname} is nonzero and nonmissing in the {bf:geoto} dataset{p_end}

{syntab:{bf:geoto} data in external stata dataset (requires {it:using})}
{synopt:{opt usingvar:list(Latvar Longvar)}}Latitude and longitude {it:varnames} in the {bf:geoto} dataset{p_end}

{syntab:{bf:geoto} data in a matrix}
{synopt:{opt geotomat:rix(matrixname)}}Matrix containing {bf:geoto} data{p_end}

{synoptline}
{p2colreset}{...}
{p 4 6 2}
{it:Latvar} and {it:Longvar} are specified in degrees North and East, respectively.{p_end}
{p 4 6 2}
{it:matrixname} should include latitude and longitude in the first and second columns, respectively.{p_end}

{marker description}{...}
{title:Description}


{pstd}
{cmd:HaversineDistance} evaluates functions of the distance between points on the surface 
of the Earth using geocoordinates expressed as latitude (degrees North) and longitude (degrees East). 
For each observation {bf:geofrom} dataset each function in {it:clist} is evaluated over all observations
in the {bf:geoto} dataset and the results placed in corresponding {it:newvar}. 

{pstd}
The {bf:geofrom} dataset consists of all observations in the current dataset allowed by
the {it:if} and {it:in} selectors and with nonmissing values of {it:Latvar} and {it:Longvar}.

{pstd}
The {bf:geoto} data may be a subset of data currently in memory, observations in an external
Stata dataset, or rows of a matrix. 

{marker remarks}{...}
{title:Remarks}

{pstd}
Distances are computed using the Haversine distance formula approximating the shape of the Earth
as an ellipsoid. For more information see 
{browse "http://www.movable-type.co.uk/scripts/gis-faq-5.1.html":www.movable-type.co.uk/scripts/gis-faq-5.1.html}.
All distance calculations are performed in Mata and are performed only once for 
each call of the program, making evaluation of multiple functions in a single 
program call computationally efficient. 

{pstd}
Obtain the latest version using

{bf:net install HaversineDistance, from(https://bitbucket.org/drofphilosophe/stata.haversinedistance/raw/master) replace}


{marker examples}{...}
{title:Examples}

{phang}{cmd:. HaversineDistance blah = CountWithin(1000) blah2= DistanceToClosest()	blah3 = MeanDistance() blah4 = WeightedMeanWithin(1000, temp) 	blah5 = closestNthIndex(1,id) 	if touse1, geofrom(lat1 lon1) geotovar(lat2 lon2) geotoselect(touse2) units(mi)}{p_end}

