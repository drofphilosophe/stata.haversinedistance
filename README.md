# README #

**HaversineDistance** is a Stata program for computing summary statistics based on the distance
between points identified by their geographic coordinates in latitude and longitude.

# Installation #
You can install the latest version of the package directly from this repository using the following in Stata:


```
#!Stata

net install HaversineDistance, from("https://bitbucket.org/drofphilosophe/stata.haversinedistance/raw/master") replace
```

Syntax and usage details are included in the associated Stata help file. 

### Contribution ###
Contributions to this project are welcome. If you are interested in contributing, contact the project administrator. Particularly helpful contributions include:

* Test cases to vet all the functions
* Functions provide other useful summary statistics
* Improved documentation in the STHLP file

# License #

Copyright 2016 James Archsmith

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.